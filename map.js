 $(function() {
	
		
		var temp
		var userZip = 1;
		var x;
        var y;
		var infowindow = null;
		var pos;
		var userCords;
        var marker;
		
		
		
		if (navigator.geolocation) {    
		
			function error(err) {
				console.warn('ERROR(' + err.code + '): ' + err.message);
			}
			
			function success(pos){
				userCords = pos.coords;
				
				
			}
		
			
			navigator.geolocation.getCurrentPosition(success, error);
			
			} else {
				alert('Geolocation is not supported in your browser');
			}
     
     var mapOptions = {
			zoom: 6,
			center: new google.maps.LatLng(53.806682, -1.555033),
			panControl: false,
			panControlOptions: {
				position: google.maps.ControlPosition.BOTTOM_LEFT
			},
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.LARGE,
				position: google.maps.ControlPosition.RIGHT_CENTER
			},
			scaleControl: false

		};
	
	
	
	
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
     
     $('#chooseZip').submit(function() { 
		
		
		 userZip = $("#textZip").val();
        console.log(userZip);
		
		var accessURL;
		
		if(userZip){
			accessURL =  "http://api.openweathermap.org/data/2.5/weather?q=" + userZip + ",uk";
		} else {
			accessURL = "http://api.openweathermap.org/data/2.5/weather?lat=" + userCords.latitude + "&lon=" + userCords.longitude;
		}
			console.log(accessURL);

			
			$.ajax({
				type: "GET",
				contentType: "application/json; charset=utf-8",
				url: accessURL,
				dataType: 'jsonp',
				success: function (data) {
                    //	-----------------	
					
					if(data.cod == "404")
                    {
                        alert("City not found");
                    }
                    else{
                        
		
                 temp = data.main.temp - 273.15;
                                
                        
                        
                   
                             
                         var rise1 = new Date(data.sys.sunrise * 1000);
                         var set1 = new Date(data.sys.sunset * 1000);
                         var rise2 = new Date(rise1);
                         var set2 = new Date(set1);
                         rise2 = rise2.toTimeString();
                         set2 = set2.toTimeString();
                        
                              

						        var rise = rise2.substring(0, 8);
						        var set = set2.substring(0, 8);
								marker = new google.maps.Marker({
                                    
									position: {lat: data.coord.lat,lng: data.coord.lon},
									map: map,
									title: 'bba',
									html: 
											'<div class="markerPop">' +
											'<h1>' + data.name + '</h1>' + 
                                            
											'<h3>' + temp.toFixed(1) + '℃ ' + data.weather[0].main + '<img src="http://openweathermap.org/img/w/'+ data.weather[0].icon + '.png">' + '</h3>' +
											
											'<p>' + 'wind speed: ' + data.wind.speed + 'm/s, ' + 'clouds:' + data.clouds.all + '%, '  + '</p>' +
                                            '<p>' + 'sunrise: ' + rise + '</p>' +
                                            '<p>' + 'sunset: ' + set + '</p>' +
											'</div>'
								});

								
							
                    infowindow = new google.maps.InfoWindow({
		content: "holding..."
	});
								
								google.maps.event.addListener(marker, 'click', function () {
									infowindow.setContent(this.html);
									infowindow.open(map, this);
								});
								
								
								
								console.log('------------');
										
							}
						
                }
				
			});

        return false; 
    });
});


                    
                    


		