 from google.appengine.api import users
import webapp2
import jinja2
import os

JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__),'templates')),extensions=['jinja2.ext.autoescape'])


class MainPage(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        logout_url = users.create_logout_url(self.request.path)
        if user:
            template = JINJA_ENVIRONMENT.get_template('index.html')
            template_values = {
                'user': user.nickname(),
                'url_logout': logout_url,
                'url_logout_text': 'Log out',
            }
            self.response.write(template.render(template_values))
        else:
            self.redirect(users.create_login_url(self.request.uri))

        def post(self):
            user = users.get_current_user()
            logout_url = users.create_logout_url(self.request.path)
            if user:
                template = JINJA_ENVIRONMENT.get_template('index.html')
                template_values = {
                    'user': user.nickname(),
                    'url_logout': logout_url,
                    'url_logout_text': 'Log out',
                    'greetings': self.request.get('content'),
                }
                self.response.write(template.render(template_values))
            else:
                self.redirect(users.create_login_url(self.request.uri))


app = webapp2.WSGIApplication([
        ('/', MainPage),
        ('/sign',MainPage)
    ], debug=True)

